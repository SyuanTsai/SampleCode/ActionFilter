﻿namespace FilterSample.Net7.Attributes;

/// <summary>
///     客製化 - Header Info
/// </summary>
[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class HeaderAttribute : Attribute
{
    /// <summary>
    /// header 欄位名
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 目前只支援 string, integer 兩個型別
    /// </summary>
    public string ParamType { get; set; }

    /// <summary>
    /// 是否必填
    /// </summary>
    public bool Required { get; set; }

    /// <summary>
    /// 預設值
    /// </summary>
    public object Default { get; set; }

    /// <summary>
    /// 說明
    /// </summary>
    public string Dec { get; set; }
}