using FilterSample.Net7.Common;
using FilterSample.Net7.Filters;
using FilterSample.Net7.Model;
using Microsoft.AspNetCore.Mvc;

namespace FilterSample.Net7.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FilterController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    [MsgResourceFilter]
    [Route("GetResource")]
    [HttpGet]
    public IEnumerable<WeatherForecast> GetResource()
    {
        return Base(out _);
    }
    
    
    // /// <summary>
    // ///     EndPoint Filter
    // /// </summary>
    // /// <returns></returns>
    // [MsgEndPointFilter]
    // [Route("GetEndPoint")]
    // [HttpGet]
    // public IEnumerable<WeatherForecast> GetEndPoint()
    // {
    //     return Base(out _);
    // }
    
    /// <summary>
    ///     Action Filter
    /// </summary>
    /// <returns></returns>
    [MsgActionFilter]
    [Route("GetAction")]
    [HttpGet]
    public IEnumerable<WeatherForecast> GetAction()
    {
        return Base(out _);
    }
    
    /// <summary>
    ///     Exception Filter
    /// </summary>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [MsgExceptionFilter]
    [Route("GetException")]
    [HttpGet]
    public IEnumerable<WeatherForecast> GetException()
    {
        throw new Exception("手動錯誤。");
    }
    
    /// <summary>
    ///     Result Filter
    /// </summary>
    /// <returns></returns>
    [MsgResultFilter]
    [Route("GetResult")]
    [HttpGet]
    public IEnumerable<WeatherForecast> GetResult()
    {
        return Base(out _);
    }
    
    /// <summary>
    ///     隔離區，讓關注點只在每個Filter
    /// </summary>
    /// <param name="result"></param>
    /// <returns></returns>
    public static IEnumerable<WeatherForecast> Base(out WeatherForecast[] result)
    {
        Log.Msg("Controller 主程式執行開始");

        result = Enumerable.Range(1, 5)
                           .Select
                           (
                               index => new WeatherForecast
                               {
                                   Date    = DateTime.Now.AddDays(index), TemperatureC = Random.Shared.Next(-20, 55)
                                 , Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                               }
                           )
                           .ToArray();

        Log.Msg("Controller 主程式執行結束準備回傳");

        return result;
    }
}