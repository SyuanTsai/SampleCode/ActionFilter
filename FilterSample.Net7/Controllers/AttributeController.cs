using FilterSample.Net7.Attributes;
using FilterSample.Net7.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FilterSample.Net7.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AttributeController : ControllerBase
{
    private static readonly string[] Summaries = {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    /// <summary>
    ///     客製化 Header Attribute
    /// </summary>
    /// <returns></returns>
    [AllowAnonymous]
    [Header
    (
        Name = "X-API-ClientID", ParamType = "string", Default = "9c79033b-334c-4cf8-ae88-bd890c8fee64", Dec = "Company"
    )]
    [Route("GetAuth")]
    [HttpGet]
    public IEnumerable<WeatherForecast> GetAuth()
    {
        var result = Enumerable.Range(1, 5)
                               .Select
                               (
                                   index => new WeatherForecast
                                   {
                                       Date    = DateTime.Now.AddDays(index), TemperatureC = Random.Shared.Next(-20, 55)
                                     , Summary = Summaries[Random.Shared.Next(Summaries.Length)]
                                   }
                               )
                               .ToArray();

        return result;
    }
}