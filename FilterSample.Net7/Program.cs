using FilterSample.Net7.Common;
using FilterSample.Net7.Controllers;
using FilterSample.Net7.Filters;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen
(
    c =>
        c.OperationFilter<HeaderFilter>()
);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}



app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGet
   (
       "/api/Filter/GetEndPoint", () =>
       {
           FilterController.Base(out _);
       }
   )
   .AddEndpointFilter<MsgEndPointFilter>();
    
    

app.Run();