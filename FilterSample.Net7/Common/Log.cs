﻿namespace FilterSample.Net7.Common;

/// <summary>
///     資訊輸出
/// </summary>
public static class Log
{
    public static void Msg(string msg)
    {
        Console.WriteLine(msg);
    }

    public static void Msg(string methodName, RouteData routeData)
    {
        var controllerName = routeData.Values["controller"];
        var actionName     = routeData.Values["action"];
        var message        = $"{methodName} controller:{controllerName} action:{actionName}";
        Console.WriteLine(message, "Filter Log");
    }
    public static void Msg(string methodName, string path)
    {
        var message        = $"{methodName} action:{path}";
        Console.WriteLine(message, "EndPoint Filter Log");
    }
}