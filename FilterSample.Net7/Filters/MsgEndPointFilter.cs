﻿using FilterSample.Net7.Common;

namespace FilterSample.Net7.Filters;

/// <summary>
///     行為 Filter
/// </summary>
public class MsgEndPointFilter :Attribute, IEndpointFilter
{
    /// <summary>
    /// Implements the core logic associated with the filter given a <see cref="T:Microsoft.AspNetCore.Http.EndpointFilterInvocationContext" />
    /// and the next filter to call in the pipeline.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Http.EndpointFilterInvocationContext" /> associated with the current request/response.</param>
    /// <param name="next">The next filter in the pipeline.</param>
    /// <returns>An awaitable result of calling the handler and apply
    /// any modifications made by filters in the pipeline.</returns>
    public async ValueTask<object?> InvokeAsync(EndpointFilterInvocationContext context, EndpointFilterDelegate next)
    {
        var request = context.HttpContext.Request.Path;

        if (request.HasValue)
        {
            
            Log.Msg("EndpointFilter - beforeInvoke", request.Value);
            var result = await next(context);
            Log.Msg("EndpointFilter - afterInvoke", request.Value);
        }

        return await next(context);
    }
}