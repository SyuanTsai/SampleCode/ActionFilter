﻿using FilterSample.Net7.Attributes;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace FilterSample.Net7.Filters;

public class HeaderFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        //--20200513 Benson
        //--這操作太騷氣…求大神提供簡單一點的寫法
        var headers = context.ApiDescription.CustomAttributes().Where(x => x.GetType() == typeof(HeaderAttribute)).Select(x => (HeaderAttribute)x).ToList();

        if (!headers.Any())
        {
            return;
        }

        operation.Parameters ??= new List<OpenApiParameter>();

        foreach (var h in headers)
        {
            var openApiParameter = new OpenApiParameter
            {
                 Name         = h.Name
               , In          = ParameterLocation.Header
               , Description = h.Dec
               , Required    = true
            };

            var schema = new OpenApiSchema {Type = h.ParamType};

            switch (h.ParamType.ToLower())
            {
                case "string":

                {
                    if (h.Default != null)
                    {
                        schema.Default = new OpenApiString(h.Default.ToString());
                    }

                    break;
                }

                case "integer":

                {
                    if (h.Default != null)
                    {
                        if (int.TryParse(h.Default.ToString(), out var defaultInt))
                        {
                            schema.Default = new OpenApiInteger(defaultInt);
                        }
                    }

                    break;
                }
            }

            openApiParameter.Schema = schema;

            operation.Parameters.Add(openApiParameter);
        }

    }
}