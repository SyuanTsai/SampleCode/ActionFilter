﻿using FilterSample.Net7.Common;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterSample.Net7.Filters;

/// <summary>
///     行為 Filter
/// </summary>
public class MsgActionFilter : ActionFilterAttribute
{
    /// <summary>
    /// Called before the action executes, after model binding is complete.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext" />.</param>
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        Log.Msg("ActionFilter - OnActionExecuting", context.RouteData);
    }

    /// <summary>
    /// Called after the action executes, before the action result.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ActionExecutedContext" />.</param>
    public override void OnActionExecuted(ActionExecutedContext context)
    {
        Log.Msg("ActionFilter - OnActionExecuted", context.RouteData);
    }
}