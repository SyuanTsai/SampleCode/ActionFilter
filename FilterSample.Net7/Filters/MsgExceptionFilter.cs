﻿using FilterSample.Net7.Common;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterSample.Net7.Filters;

public class MsgExceptionFilter : ExceptionFilterAttribute
{
    public MsgExceptionFilter()
    {
    }

    /// <summary>
    /// Called after an action has thrown an <see cref="T:System.Exception" />.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ExceptionContext" />.</param>
    public override void OnException(ExceptionContext context)
    {
        Log.Msg("ExceptionFilter - OnException", context.RouteData);
    }
}