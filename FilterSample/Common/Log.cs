﻿namespace FilterSample.Common;

/// <summary>
///     資訊輸出
/// </summary>
public static class Log
{
    public static void Msg(string msg)
    {
        Console.WriteLine(msg);
    }

    public static void Msg(string methodName, RouteData routeData)
    {
        var controllerName = routeData.Values["controller"];
        var actionName     = routeData.Values["action"];
        var message        = $"{methodName} controller:{controllerName} action:{actionName}";
        Console.WriteLine(message, "Action Filter Log");
    }
    public static void ExMsg(string methodName, RouteData routeData)
    {
        var controllerName = routeData.Values["controller"];
        var actionName     = routeData.Values["action"];
        var message        = $"{methodName} controller:{controllerName} action:{actionName}";
        Console.WriteLine(message, "Exception Filter Log");
    }
}