﻿using FilterSample.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;


namespace FilterSample.Filters;

public class MsgAuthorizationFilter : AuthorizeAttribute,  IAuthorizationFilter
{
    public MsgAuthorizationFilter()
    {
    }

    /// <summary>
    /// Called early in the filter pipeline to confirm request is authorized.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.AuthorizationFilterContext" />.</param>
    public void OnAuthorization(AuthorizationFilterContext context)
    {
        Log.Msg("Authorization - OnAuthorization", context.RouteData);
    }
}