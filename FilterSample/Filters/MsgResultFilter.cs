﻿using FilterSample.Common;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterSample.Filters;

public class MsgResultFilter : Attribute, IResultFilter
{
    /// <summary>Called before the action result executes.</summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ResultExecutingContext" />.</param>
    public void OnResultExecuting(ResultExecutingContext context)
    {
        Log.Msg("ResultFilter - OnResultExecuting", context.RouteData);
    }

    /// <summary>Called after the action result executes.</summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ResultExecutedContext" />.</param>
    public void OnResultExecuted(ResultExecutedContext   context)
    {
        Log.Msg("ResultFilter - OnResultExecuted", context.RouteData);
    }
}