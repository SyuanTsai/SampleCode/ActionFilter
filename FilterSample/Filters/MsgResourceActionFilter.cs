﻿using FilterSample.Common;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FilterSample.Filters;

public class MsgResourceActionFilter : Attribute, IResourceFilter
{
    /// <summary>
    /// Executes the resource filter. Called before execution of the remainder of the pipeline.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ResourceExecutingContext" />.</param>
    public void OnResourceExecuting(ResourceExecutingContext context)
    {
        Log.Msg("ResourceFilter - OnResourceExecuting", context.RouteData);
    }

    /// <summary>
    /// Executes the resource filter. Called after execution of the remainder of the pipeline.
    /// </summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ResourceExecutedContext" />.</param>
    public void OnResourceExecuted(ResourceExecutedContext context)
    {
        Log.Msg("ResourceFilter - OnResourceExecuted", context.RouteData);
    }
}